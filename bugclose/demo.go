package main

import (
	"encoding/json"
	"fmt"
	"scmTool/bugclose/bugcloseapi"
)

func main() {
	b := bugcloseapi.NewClient("https://www.bugclose.com/cgi/", "kangjia.feng@megatronix.co", "Fkj2022@")

	// // 查询issues
	// req := &bugcloseapi.BugQueryReq{
	// 	ProjectId:  "56989",
	// 	AssignToId: "101494",
	// 	State:      "Fixed",
	// }
	// issues := b.IssueApi.BugQuery(req)
	// issue_json, _ := json.Marshal(issues.Root[0])
	// fmt.Println(string(issue_json))

	// // 获取指定项目的所有用户
	// users := b.UserApi.GetProjectUsers("56989")
	// for _, user := range users {
	// 	fmt.Println(user.UserId)
	// 	fmt.Println(user.User.Email)
	// 	fmt.Println(user.DisplayName)
	// }

	// 获取issue的具体信息
	issueDetails := b.IssueApi.GetIssueDetails("56989", "65302")
	issueDetailsJson, _ := json.Marshal(issueDetails.Root)
	fmt.Println(string(issueDetailsJson))

}
