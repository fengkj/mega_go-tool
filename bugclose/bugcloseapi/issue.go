package bugcloseapi

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type IssueApiService service

type IssueDetails struct {
	Root struct {
		Active       bool     `json:"active"`
		AllowActions []string `json:"allowActions"`
		AssignToId   int      `json:"assignToId"`
		AssignedById int      `json:"assignedById"`
		AssignedTime int64    `json:"assignedTime"`
		BugType      string   `json:"bugType"`
		Confirmed    bool     `json:"confirmed"`
		CreatedById  int      `json:"createdById"`
		CreatedIP    string   `json:"createdIP"`
		CreatedTime  int64    `json:"createdTime"`
		CustomerId   int      `json:"customerId"`
		Detail       Detail   `json:"detail"`
		Environment  string   `json:"environment"`
		FixedById    int      `json:"fixedById"`
		Histories    []struct {
			Action       string   `json:"action"`
			AllowActions []string `json:"allowActions"`
			AssignToId   int      `json:"assignToId"`
			BugId        int      `json:"bugId"`
			BugType      string   `json:"bugType"`
			CreatedIP    string   `json:"createdIP"`
			Detail       struct {
				BugId        int `json:"bugId"`
				CustomValues struct {
					Field1 string `json:"58"`
					Field2 string `json:"59"`
					Field3 string `json:"60"`
					Field4 string `json:"61"`
				} `json:"customValues"`
				Description string `json:"description"`
				Id          int    `json:"id"`
				Markdown    bool   `json:"markdown"`
			} `json:"detail,omitempty"`
			Environment  string        `json:"environment,omitempty"`
			Id           int           `json:"id"`
			Markdown     bool          `json:"markdown"`
			ModifiedById int           `json:"modifiedById"`
			ModifiedTime int64         `json:"modifiedTime"`
			Module       string        `json:"module,omitempty"`
			Percent      int           `json:"percent"`
			PlanVersion  string        `json:"planVersion,omitempty"`
			Priority     string        `json:"priority"`
			ProductId    int           `json:"productId"`
			SprintId     int           `json:"sprintId"`
			State        string        `json:"state"`
			SubTypeId    int           `json:"subTypeId"`
			TagIds       []int         `json:"tagIds,omitempty"`
			Title        string        `json:"title,omitempty"`
			Treatment    string        `json:"treatment"`
			Value        int           `json:"value"`
			Version      string        `json:"version,omitempty"`
			Workload     int           `json:"workload"`
			Attachments  []Attachments `json:"attachments,omitempty"`
			Comments     string        `json:"comments,omitempty"`
		} `json:"histories"`
		Id               int           `json:"id"`
		LastAssignedById int           `json:"lastAssignedById"`
		LastAssignedTime int64         `json:"lastAssignedTime"`
		ModifiedById     int           `json:"modifiedById"`
		ModifiedTime     int64         `json:"modifiedTime"`
		Module           string        `json:"module"`
		NoPass           bool          `json:"noPass"`
		Number           int           `json:"number"`
		ParentId         int           `json:"parentId"`
		Percent          int           `json:"percent"`
		PostTasks        []interface{} `json:"postTasks"`
		PreTasks         []interface{} `json:"preTasks"`
		Priority         string        `json:"priority"`
		ProductId        int           `json:"productId"`
		ProjectId        int           `json:"projectId"`
		RelatedBugs      []struct {
			Active           bool   `json:"active"`
			AssignToId       int    `json:"assignToId"`
			AssignedById     int    `json:"assignedById"`
			AssignedTime     int64  `json:"assignedTime"`
			BugType          string `json:"bugType"`
			Confirmed        bool   `json:"confirmed"`
			CreatedById      int    `json:"createdById"`
			CreatedIP        string `json:"createdIP"`
			CreatedTime      int64  `json:"createdTime"`
			CustomerId       int    `json:"customerId"`
			Environment      string `json:"environment"`
			FixedById        int    `json:"fixedById"`
			Id               int    `json:"id"`
			LastAssignedById int    `json:"lastAssignedById"`
			LastAssignedTime int64  `json:"lastAssignedTime"`
			ModifiedById     int    `json:"modifiedById"`
			ModifiedTime     int64  `json:"modifiedTime"`
			Module           string `json:"module"`
			NoPass           bool   `json:"noPass"`
			Number           int    `json:"number"`
			ParentId         int    `json:"parentId"`
			Percent          int    `json:"percent"`
			PlanVersion      string `json:"planVersion"`
			Priority         string `json:"priority"`
			Product          struct {
				BugAssignToId     int    `json:"bugAssignToId"`
				BugVerifierId     int    `json:"bugVerifierId"`
				Closed            bool   `json:"closed"`
				DefaultAssignToId int    `json:"defaultAssignToId"`
				Id                int    `json:"id"`
				Name              string `json:"name"`
				ProjectId         int    `json:"projectId"`
				RequireAssignToId int    `json:"requireAssignToId"`
				RequireVerifierId int    `json:"requireVerifierId"`
				TaskAssignToId    int    `json:"taskAssignToId"`
				TaskVerifierId    int    `json:"taskVerifierId"`
			} `json:"product"`
			ProductId int `json:"productId"`
			Project   struct {
				AllowConfig             bool   `json:"allowConfig"`
				BugInfoEnabled          bool   `json:"bugInfoEnabled"`
				Closed                  bool   `json:"closed"`
				ConfirmEnabled          bool   `json:"confirmEnabled"`
				CreatedTime             int64  `json:"createdTime"`
				CustomEnabled           bool   `json:"customEnabled"`
				CustomerEnabled         bool   `json:"customerEnabled"`
				DisplayImageNameEnabled bool   `json:"displayImageNameEnabled"`
				DisplayUserInfoEnabled  bool   `json:"displayUserInfoEnabled"`
				DueDate                 int64  `json:"dueDate"`
				EnvEnabled              bool   `json:"envEnabled"`
				EvaEnabled              bool   `json:"evaEnabled"`
				FocusEnabled            bool   `json:"focusEnabled"`
				Id                      int    `json:"id"`
				ModuleEnabled           bool   `json:"moduleEnabled"`
				Name                    string `json:"name"`
				OwnerExcluded           bool   `json:"ownerExcluded"`
				OwnerId                 int    `json:"ownerId"`
				PartnerEnabled          bool   `json:"partnerEnabled"`
				PlanDate                int64  `json:"planDate"`
				Professional            bool   `json:"professional"`
				ReadOnly                bool   `json:"readOnly"`
				ReviewEnabled           bool   `json:"reviewEnabled"`
				SprintEnabled           bool   `json:"sprintEnabled"`
				State                   string `json:"state"`
				SubTaskEnabled          bool   `json:"subTaskEnabled"`
				TagEnabled              bool   `json:"tagEnabled"`
				TestCaseEnabled         bool   `json:"testCaseEnabled"`
				UserNumber              int    `json:"userNumber"`
				VersionEnabled          bool   `json:"versionEnabled"`
				WeekBeginMonday         bool   `json:"weekBeginMonday"`
			} `json:"project"`
			ProjectId    int    `json:"projectId"`
			Reopened     bool   `json:"reopened"`
			Reviewed     bool   `json:"reviewed"`
			SprintId     int    `json:"sprintId"`
			State        string `json:"state"`
			SubTypeId    int    `json:"subTypeId"`
			TagIds       []int  `json:"tagIds"`
			TestCaseId   int    `json:"testCaseId"`
			TestTaskId   int    `json:"testTaskId"`
			Title        string `json:"title"`
			Treatment    string `json:"treatment"`
			Value        int    `json:"value"`
			VerifiedTime int64  `json:"verifiedTime"`
			VerifierId   int    `json:"verifierId"`
			Version      string `json:"version"`
			Workload     int    `json:"workload"`
		} `json:"relatedBugs"`
		Reopened   bool          `json:"reopened"`
		Reviewed   bool          `json:"reviewed"`
		SprintId   int           `json:"sprintId"`
		State      string        `json:"state"`
		SubBugs    []interface{} `json:"subBugs"`
		SubTypeId  int           `json:"subTypeId"`
		TagIds     []int         `json:"tagIds"`
		TestCaseId int           `json:"testCaseId"`
		TestTaskId int           `json:"testTaskId"`
		Title      string        `json:"title"`
		Treatment  string        `json:"treatment"`
		Value      int           `json:"value"`
		VerifierId int           `json:"verifierId"`
		Version    string        `json:"version"`
		Workload   int           `json:"workload"`
	} `json:"root"`
	Success bool `json:"success"`
}
type CustomValues struct {
}

type Detail struct {
	Attachments    []Attachments `json:"attachments"`
	CustomValues   CustomValues  `json:"customValues"`
	Description    string        `json:"description"`
	Id             int           `json:"id"`
	Markdown       bool          `json:"markdown"`
	ParticipantIds []int         `json:"participantIds"`
}

type Attachments struct {
	CreatedIP        string `json:"createdIP"`
	Id               int    `json:"id"`
	OriginalFileName string `json:"originalFileName"`
	ProjectId        int    `json:"projectId"`
	UploadTime       int64  `json:"uploadTime"`
	Url              string `json:"url"`
	UserId           int    `json:"userId"`
}

type BugQueryReq struct {
	ProjectId  string `json:"projectId"`
	AssignToId string `json:"assignToId"`
	State      string `json:"states"`
	MaxResults string `json:"maxResults"`
}

type BugQueryResponse struct {
	Root []struct {
		Active           bool          `json:"active"`
		AllowActions     []string      `json:"allowActions"`
		AssignToId       int           `json:"assignToId"`
		AssignedById     int           `json:"assignedById"`
		AssignedTime     int64         `json:"assignedTime"`
		BugType          string        `json:"bugType"`
		Confirmed        bool          `json:"confirmed"`
		CreatedById      int           `json:"createdById"`
		CreatedIP        string        `json:"createdIP"`
		CreatedTime      int64         `json:"createdTime"`
		CustomerId       int           `json:"customerId"`
		Environment      string        `json:"environment"`
		FixedById        int           `json:"fixedById"`
		Id               int           `json:"id"`
		LastAssignedById int           `json:"lastAssignedById"`
		LastAssignedTime int64         `json:"lastAssignedTime,omitempty"`
		ModifiedById     int           `json:"modifiedById"`
		ModifiedTime     int64         `json:"modifiedTime"`
		Module           string        `json:"module"`
		NoPass           bool          `json:"noPass"`
		Number           int           `json:"number"`
		ParentId         int           `json:"parentId"`
		Percent          int           `json:"percent"`
		PlanVersion      string        `json:"planVersion"`
		Priority         string        `json:"priority"`
		ProductId        int           `json:"productId"`
		ProjectId        int           `json:"projectId"`
		Reopened         bool          `json:"reopened"`
		Reviewed         bool          `json:"reviewed"`
		SprintId         int           `json:"sprintId"`
		State            string        `json:"state"`
		SubTypeId        int           `json:"subTypeId"`
		TagIds           []interface{} `json:"tagIds"`
		TestCaseId       int           `json:"testCaseId"`
		TestTaskId       int           `json:"testTaskId"`
		Title            string        `json:"title"`
		Treatment        string        `json:"treatment"`
		Value            int           `json:"value"`
		VerifierId       int           `json:"verifierId"`
		Version          string        `json:"version"`
		Workload         int           `json:"workload"`
		FixedTime        int64         `json:"fixedTime,omitempty"`
	} `json:"root"`
	Success bool `json:"success"`
}

type BugCountResponse struct {
	Root   int  `json:"root"`
	Status bool `json:"success"`
}

func (i *IssueApiService) BugCount(ProjectId string) (int, error) {
	httpUrl := i.client.cfg.Host + "bug/count"
	var t BugCountResponse
	_, _, errors := i.client.cfg.HTTPClient.
		Post(httpUrl).
		Param("token", i.client.cfg.Token).
		Param("projectId", ProjectId).
		EndStruct(&t)
	if errors != nil {
		panic(errors)
	}
	return t.Root, nil
}

func (i *IssueApiService) BugQuery(r *BugQueryReq) BugQueryResponse {
	var issuereq BugQueryResponse
	httpUrl := i.client.cfg.Host + "bug/query"
	if r.MaxResults == "" {
		r.MaxResults = "0"
	}
	body, writer, _ := i.client.marshalMap(r)
	req, _ := http.NewRequest(http.MethodPost, httpUrl, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Add("token", i.client.cfg.Token)
	res, err := http.DefaultClient.Do(req)
	data, _ := io.ReadAll(res.Body)
	if err != nil {
		panic(err)
	} else {
		err := json.Unmarshal(data, &issuereq)
		if err != nil {
			panic(err)
		}
	}
	return issuereq
}

func (i *IssueApiService) GetIssueDetails(ProjectId, IssueId string) IssueDetails {
	var ids IssueDetails
	httpUrl := i.client.cfg.Host + "bug/get"
	resp, _, errors := i.client.cfg.HTTPClient.
		Post(httpUrl).
		Param("token", i.client.cfg.Token).
		Param("projectId", ProjectId).
		Param("id", IssueId).
		EndStruct(&ids)
	if errors != nil {
		panic(errors)
	}
	fmt.Println(resp)
	return ids
}
