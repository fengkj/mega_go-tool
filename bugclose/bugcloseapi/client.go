package bugcloseapi

import (
	"bytes"
	"encoding/json"
	"mime/multipart"

	"github.com/parnurzeal/gorequest"
)

type APIClient struct {
	cfg    *Configuration
	common service // Reuse a single struct instead of allocating one for each service on the heap.

	// API Services

	AuthApi *AuthApiService

	IssueApi *IssueApiService

	ProjectApi *ProjectApiService

	UserApi *UserApiService

	// MultimediafileApi *MultimediafileApiService

	// UserinfoApi *UserinfoApiService
}

type service struct {
	client *APIClient
}

type Configuration struct {
	Host       string `json:"host"`
	Username   string `json:"username"`
	Passwrod   string `json:"password"`
	Token      string `json:"token"`
	HTTPClient *gorequest.SuperAgent
}

func NewClient(host, username, password string) *APIClient {
	cfg := &Configuration{
		Host:     host,
		Username: username,
		Passwrod: password,
	}
	if cfg.HTTPClient == nil {
		cfg.HTTPClient = gorequest.New()
	}

	c := &APIClient{}
	c.cfg = cfg
	c.common.client = c

	// API Services
	c.AuthApi = (*AuthApiService)(&c.common)

	// 初始化token
	// token, err := c.AuthApi.GetToken()
	// if err != nil {
	// 	panic(err)
	// }
	cfg.Token = "2cf34803efd042c5b3949c4bea726d5a"

	c.IssueApi = (*IssueApiService)(&c.common)
	c.ProjectApi = (*ProjectApiService)(&c.common)
	c.UserApi = (*UserApiService)(&c.common)
	// c.MultimediafileApi = (*MultimediafileApiService)(&c.common)
	// c.UserinfoApi = (*UserinfoApiService)(&c.common)

	return c
}

func (c *APIClient) marshalMap(t interface{}) (*bytes.Buffer, *multipart.Writer, error) {
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	defer writer.Close()
	marshal, err := json.Marshal(&t)
	if err != nil {
		return nil, nil, err
	}
	var m map[string]interface{}
	_ = json.Unmarshal(marshal, &m)
	for k, v := range m {
		if v != "" {
			_ = writer.WriteField(k, v.(string))
		}
	}
	return body, writer, nil
}
