package bugcloseapi

import "fmt"

type AuthApiService service

type TokenResponse struct {
	Token  string `json:"root"`
	Status bool   `json:"success"`
}

// 获取token
func (a *AuthApiService) GetToken() (string, []error) {
	httpUrl := a.client.cfg.Host + "user/login"
	var t TokenResponse
	resp, _, errors := a.client.cfg.HTTPClient.
		Post(httpUrl).
		Param("email", a.client.cfg.Username).
		Param("password", a.client.cfg.Passwrod).
		EndStruct(&t)
	if errors != nil {
		panic(errors)
	}
	fmt.Println(resp)
	return t.Token, nil
}
