package bugcloseapi

import "fmt"

type ProjectApiService service

type ProjectDetails struct {
	Root struct {
		AllowConfig     bool          `json:"allowConfig"`
		BugInfoEnabled  bool          `json:"bugInfoEnabled"`
		Closed          bool          `json:"closed"`
		ConfirmEnabled  bool          `json:"confirmEnabled"`
		CreatedTime     int64         `json:"createdTime"`
		CustomEnabled   bool          `json:"customEnabled"`
		CustomFields    []interface{} `json:"customFields"`
		CustomerEnabled bool          `json:"customerEnabled"`
		Customers       []interface{} `json:"customers"`
		Detail          struct {
			Description string `json:"description"`
			Id          int    `json:"id"`
		} `json:"detail"`
		DisplayImageNameEnabled bool   `json:"displayImageNameEnabled"`
		DisplayUserInfoEnabled  bool   `json:"displayUserInfoEnabled"`
		EnvEnabled              bool   `json:"envEnabled"`
		EvaEnabled              bool   `json:"evaEnabled"`
		FocusEnabled            bool   `json:"focusEnabled"`
		HeadImageUrl            string `json:"headImageUrl"`
		Id                      int    `json:"id"`
		ModuleEnabled           bool   `json:"moduleEnabled"`
		Name                    string `json:"name"`
		Owner                   struct {
			Email        string `json:"email"`
			HeadImageId  int    `json:"headImageId"`
			HeadImageUrl string `json:"headImageUrl"`
			Id           int    `json:"id"`
			UserName     string `json:"userName"`
		} `json:"owner"`
		OwnerExcluded  bool  `json:"ownerExcluded"`
		OwnerId        int   `json:"ownerId"`
		PartnerEnabled bool  `json:"partnerEnabled"`
		ProExpireDate  int64 `json:"proExpireDate"`
		Products       []struct {
			BugAssignToId     int    `json:"bugAssignToId"`
			BugVerifierId     int    `json:"bugVerifierId"`
			Closed            bool   `json:"closed"`
			DefaultAssignToId int    `json:"defaultAssignToId"`
			Id                int    `json:"id"`
			Initial           string `json:"initial"`
			Modules           []struct {
				Id        int    `json:"id"`
				Initial   string `json:"initial"`
				Name      string `json:"name"`
				Pinyin    string `json:"pinyin"`
				ProductId int    `json:"productId"`
				ProjectId int    `json:"projectId"`
			} `json:"modules,omitempty"`
			Name              string `json:"name"`
			Pinyin            string `json:"pinyin"`
			ProjectId         int    `json:"projectId"`
			RequireAssignToId int    `json:"requireAssignToId"`
			RequireVerifierId int    `json:"requireVerifierId"`
			TaskAssignToId    int    `json:"taskAssignToId"`
			TaskVerifierId    int    `json:"taskVerifierId"`
		} `json:"products"`
		Professional   bool                   `json:"professional"`
		ProjectType    string                 `json:"projectType"`
		ProjectUsers   []ProjectUsersResponse `json:"projectUsers"`
		ReadOnly       bool                   `json:"readOnly"`
		ReviewEnabled  bool                   `json:"reviewEnabled"`
		SprintEnabled  bool                   `json:"sprintEnabled"`
		Sprints        []interface{}          `json:"sprints"`
		State          string                 `json:"state"`
		SubTaskEnabled bool                   `json:"subTaskEnabled"`
		SubTypes       []interface{}          `json:"subTypes"`
		TagEnabled     bool                   `json:"tagEnabled"`
		Tags           []struct {
			Color       string `json:"color"`
			GroupName   string `json:"groupName"`
			GroupPinyin string `json:"groupPinyin"`
			Id          int    `json:"id"`
			Name        string `json:"name"`
			Pinyin      string `json:"pinyin"`
			ProjectId   int    `json:"projectId"`
		} `json:"tags"`
		Templates       []interface{} `json:"templates"`
		TestCaseEnabled bool          `json:"testCaseEnabled"`
		TestGroups      []interface{} `json:"testGroups"`
		TestPlans       []interface{} `json:"testPlans"`
		UserNumber      int           `json:"userNumber"`
		UserRoles       []struct {
			CanAddModule       bool   `json:"canAddModule"`
			CanAddNotice       bool   `json:"canAddNotice"`
			CanAddTestCase     bool   `json:"canAddTestCase"`
			CanAddTestPlan     bool   `json:"canAddTestPlan"`
			CanAddTestTask     bool   `json:"canAddTestTask"`
			CanAddVersion      bool   `json:"canAddVersion"`
			CanAssignBug       bool   `json:"canAssignBug"`
			CanCancelFocusBug  bool   `json:"canCancelFocusBug"`
			CanCloseBug        bool   `json:"canCloseBug"`
			CanCommentBug      bool   `json:"canCommentBug"`
			CanCommentNotice   bool   `json:"canCommentNotice"`
			CanCommentTestCase bool   `json:"canCommentTestCase"`
			CanCommentVersion  bool   `json:"canCommentVersion"`
			CanDeleteBug       bool   `json:"canDeleteBug"`
			CanDeleteComment   bool   `json:"canDeleteComment"`
			CanDeleteModule    bool   `json:"canDeleteModule"`
			CanDeleteNotice    bool   `json:"canDeleteNotice"`
			CanDeleteTestCase  bool   `json:"canDeleteTestCase"`
			CanDeleteTestPlan  bool   `json:"canDeleteTestPlan"`
			CanDeleteTestTask  bool   `json:"canDeleteTestTask"`
			CanDeleteVersion   bool   `json:"canDeleteVersion"`
			CanExportBug       bool   `json:"canExportBug"`
			CanExportTestCase  bool   `json:"canExportTestCase"`
			CanInviteFocusBug  bool   `json:"canInviteFocusBug"`
			CanModifyBug       bool   `json:"canModifyBug"`
			CanModifyComment   bool   `json:"canModifyComment"`
			CanModifyModule    bool   `json:"canModifyModule"`
			CanModifyNotice    bool   `json:"canModifyNotice"`
			CanModifyTestCase  bool   `json:"canModifyTestCase"`
			CanModifyTestPlan  bool   `json:"canModifyTestPlan"`
			CanModifyTestTask  bool   `json:"canModifyTestTask"`
			CanModifyVersion   bool   `json:"canModifyVersion"`
			CanReviewBug       bool   `json:"canReviewBug"`
			CanSetTopNotice    bool   `json:"canSetTopNotice"`
			CanStatBug         bool   `json:"canStatBug"`
			CanStatTestCase    bool   `json:"canStatTestCase"`
			CanStatTestTask    bool   `json:"canStatTestTask"`
			CanSubmitBug       bool   `json:"canSubmitBug"`
			CanViewBug         bool   `json:"canViewBug"`
			CanViewNotice      bool   `json:"canViewNotice"`
			CanViewTestCase    bool   `json:"canViewTestCase"`
			CanViewTestPlan    bool   `json:"canViewTestPlan"`
			CanViewTestTask    bool   `json:"canViewTestTask"`
			CanViewVersion     bool   `json:"canViewVersion"`
			Id                 int    `json:"id"`
			Name               string `json:"name"`
			Pinyin             string `json:"pinyin"`
			ProjectId          int    `json:"projectId"`
		} `json:"userRoles"`
		VersionEnabled  bool `json:"versionEnabled"`
		WeekBeginMonday bool `json:"weekBeginMonday"`
	} `json:"root"`
	Success bool `json:"success"`
}

func (p *ProjectApiService) GetProjectDetail(projectId string) ProjectDetails {
	var details ProjectDetails
	httpUrl := p.client.cfg.Host + "project/get"
	_, _, errors := p.client.cfg.HTTPClient.
		Post(httpUrl).
		Param("token", p.client.cfg.Token).
		Param("id", projectId).
		EndStruct(&details)
	if errors != nil {
		fmt.Println(errors)
	}
	return details
}
