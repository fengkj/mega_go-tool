package bugcloseapi

type UserApiService service

type ProjectUsersResponse struct {
	AppNotifyEnabled   bool   `json:"appNotifyEnabled"`
	CanAddModule       bool   `json:"canAddModule"`
	CanAddNotice       bool   `json:"canAddNotice"`
	CanAddTestCase     bool   `json:"canAddTestCase"`
	CanAddTestPlan     bool   `json:"canAddTestPlan"`
	CanAddTestTask     bool   `json:"canAddTestTask"`
	CanAddVersion      bool   `json:"canAddVersion"`
	CanAssignBug       bool   `json:"canAssignBug"`
	CanCancelFocusBug  bool   `json:"canCancelFocusBug"`
	CanCloseBug        bool   `json:"canCloseBug"`
	CanCommentBug      bool   `json:"canCommentBug"`
	CanCommentNotice   bool   `json:"canCommentNotice"`
	CanCommentTestCase bool   `json:"canCommentTestCase"`
	CanCommentVersion  bool   `json:"canCommentVersion"`
	CanDeleteBug       bool   `json:"canDeleteBug"`
	CanDeleteComment   bool   `json:"canDeleteComment"`
	CanDeleteModule    bool   `json:"canDeleteModule"`
	CanDeleteNotice    bool   `json:"canDeleteNotice"`
	CanDeleteTestCase  bool   `json:"canDeleteTestCase"`
	CanDeleteTestPlan  bool   `json:"canDeleteTestPlan"`
	CanDeleteTestTask  bool   `json:"canDeleteTestTask"`
	CanDeleteVersion   bool   `json:"canDeleteVersion"`
	CanExportBug       bool   `json:"canExportBug"`
	CanExportTestCase  bool   `json:"canExportTestCase"`
	CanInviteFocusBug  bool   `json:"canInviteFocusBug"`
	CanModifyBug       bool   `json:"canModifyBug"`
	CanModifyComment   bool   `json:"canModifyComment"`
	CanModifyModule    bool   `json:"canModifyModule"`
	CanModifyNotice    bool   `json:"canModifyNotice"`
	CanModifyTestCase  bool   `json:"canModifyTestCase"`
	CanModifyTestPlan  bool   `json:"canModifyTestPlan"`
	CanModifyTestTask  bool   `json:"canModifyTestTask"`
	CanModifyVersion   bool   `json:"canModifyVersion"`
	CanReviewBug       bool   `json:"canReviewBug"`
	CanSetTopNotice    bool   `json:"canSetTopNotice"`
	CanStatBug         bool   `json:"canStatBug"`
	CanStatTestCase    bool   `json:"canStatTestCase"`
	CanStatTestTask    bool   `json:"canStatTestTask"`
	CanSubmitBug       bool   `json:"canSubmitBug"`
	CanViewBug         bool   `json:"canViewBug"`
	CanViewNotice      bool   `json:"canViewNotice"`
	CanViewTestCase    bool   `json:"canViewTestCase"`
	CanViewTestPlan    bool   `json:"canViewTestPlan"`
	CanViewTestTask    bool   `json:"canViewTestTask"`
	CanViewVersion     bool   `json:"canViewVersion"`
	Disabled           bool   `json:"disabled"`
	DisplayName        string `json:"displayName"`
	EmailNotifyEnabled bool   `json:"emailNotifyEnabled"`
	GroupName          string `json:"groupName"`
	GroupPinyin        string `json:"groupPinyin"`
	Initial            string `json:"initial"`
	Pinyin             string `json:"pinyin"`
	ProjectId          int    `json:"projectId"`
	RightEnabled       bool   `json:"rightEnabled"`
	RoleId             int    `json:"roleId"`
	User               struct {
		Email            string `json:"email"`
		HeadImageId      int    `json:"headImageId"`
		HeadImageUrl     string `json:"headImageUrl"`
		Id               int    `json:"id"`
		UserName         string `json:"userName"`
		AssignToMeAlert  bool   `json:"assignToMeAlert,omitempty"`
		AssignToMeMail   bool   `json:"assignToMeMail,omitempty"`
		BugModifiedAlert bool   `json:"bugModifiedAlert,omitempty"`
		BugModifiedMail  bool   `json:"bugModifiedMail,omitempty"`
		CanCreateProject bool   `json:"canCreateProject,omitempty"`
		EmailVerified    bool   `json:"emailVerified,omitempty"`
		FocusAlert       bool   `json:"focusAlert,omitempty"`
		FocusMail        bool   `json:"focusMail,omitempty"`
		InviteAlert      bool   `json:"inviteAlert,omitempty"`
		InviteMail       bool   `json:"inviteMail,omitempty"`
		LastLoginIP      string `json:"lastLoginIP,omitempty"`
		LastLoginTime    int64  `json:"lastLoginTime,omitempty"`
		NoticeAlert      bool   `json:"noticeAlert,omitempty"`
		NoticeMail       bool   `json:"noticeMail,omitempty"`
		PostponedAlert   bool   `json:"postponedAlert,omitempty"`
		PostponedMail    bool   `json:"postponedMail,omitempty"`
		RegisterIP       string `json:"registerIP,omitempty"`
		RegisterTime     int64  `json:"registerTime,omitempty"`
		RejectedAlert    bool   `json:"rejectedAlert,omitempty"`
		RejectedMail     bool   `json:"rejectedMail,omitempty"`
		RequireAlert     bool   `json:"requireAlert,omitempty"`
		RequireMail      bool   `json:"requireMail,omitempty"`
		UrgentAlert      bool   `json:"urgentAlert,omitempty"`
		UrgentMail       bool   `json:"urgentMail,omitempty"`
		WxNotifyEnabled  bool   `json:"wxNotifyEnabled,omitempty"`
	} `json:"user"`
	UserId          int    `json:"userId"`
	UserType        string `json:"userType"`
	WxNotifyEnabled bool   `json:"wxNotifyEnabled"`
	RemarkName      string `json:"remarkName,omitempty"`
}

func (u *UserApiService) GetProjectUsers(projectId string) []ProjectUsersResponse {
	projectDetails := u.client.ProjectApi.GetProjectDetail(projectId)
	return projectDetails.Root.ProjectUsers
}
