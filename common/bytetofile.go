package common

import (
	"bytes"
	"io"
	"os"
)

// []byte to os.file, reference embed
func ByteToFile(seq []byte) (*os.File, error) {

	// 创建临时文件
	tmpfile, err := os.CreateTemp("", "tempfile")
	if err != nil {
		return nil, err
	}

	// 将byte写进临时文件中
	_, err = io.Copy(tmpfile, bytes.NewReader(seq))
	if err != nil {
		return nil, err
	}

	// 将文件指针重置到文件开始处
	_, err = tmpfile.Seek(0, 0)
	if err != nil {
		return nil, err
	}

	return tmpfile, nil
}
