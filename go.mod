module scmTool

go 1.20

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/parnurzeal/gorequest v0.2.16 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/spf13/cobra v1.7.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/net v0.10.0 // indirect
	moul.io/http2curl v1.0.0 // indirect
)
