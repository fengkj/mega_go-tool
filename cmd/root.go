package cmd

import (
	"scmTool/cmd/baidudisk"

	"github.com/spf13/cobra"
)

var (
	// Used for flags.
	cfgFile     string
	userLicense string

	rootCmd = &cobra.Command{
		Use:     "scm-cli",
		Short:   "scm cli",
		Long:    `scm cli.`,
		Example: "./scm-cli command",
	}
)

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	rootCmd.AddCommand(baidudisk.BaiduNetDisk)
}
