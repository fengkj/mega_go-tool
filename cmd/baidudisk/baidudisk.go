package baidudisk

import (
	"fmt"
	"path/filepath"
	"scmTool/baidudisk/file"
	"strings"

	"github.com/spf13/cobra"
)

var (
	filePath    string
	uploadPath  string
	accessToken string
)

var BaiduNetDisk = &cobra.Command{
	Use:   "baidudisk",
	Short: "baiduNetDisk command",
	RunE: func(cmd *cobra.Command, args []string) error {
		return cmd.Usage()
	},
}

var FileUpload = &cobra.Command{
	Use:   "upload",
	Short: "upload localfile to baidudisk",
	RunE: func(cmd *cobra.Command, args []string) error {
		if uploadPath == "" || filePath == "" {
			return cmd.Usage()
		}
		if accessToken == "" {
			fmt.Println("***** accessToken is null, autoget from db *****")
		}
		if strings.HasSuffix(uploadPath, "/") {
			uploadPath = uploadPath + filepath.Base(filePath)
		}
		fileUploader := file.NewUploader(accessToken, filePath, uploadPath)
		_, err := fileUploader.Upload()
		if err != nil {
			fmt.Println("err:", err)
			panic(err)
		}
		return nil
	},
}

func init() {
	BaiduNetDisk.AddCommand(FileUpload)

	FileUpload.PersistentFlags().StringVarP(&accessToken, "baidudisk accesstoken", "a", "", "baidudisk accesstoken, none autoget from db")
	FileUpload.PersistentFlags().StringVarP(&uploadPath, "baidudisk file uploadpath", "p", "", "baidudisk file uploadpath with file name, only path needs to end with /")
	FileUpload.PersistentFlags().StringVarP(&filePath, "local file path with file name", "f", "", "local file path with the file name")
}
