package account

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	openapi "scmTool/baidudisk/openxpanapi"
	"strconv"
)

type Account struct {
	AccessToken string
}

type UserInfoResponse struct {
	BaiduName    string `json:"baidu_name"`
	NetdiskName  string `json:"netdisk_name"`
	AvatarUrl    string `json:"avatar_url"`
	VipType      int    `json:"vip_type"`
	Uk           int    `json:"uk"` //uk字段对应auth.UserInfo方法返回的user_id
	ErrorCode    int    `json:"errno"`
	ErrorMsg     string `json:"errmsg"`
	RequestID    int
	RequestIDStr string `json:"request_id"` //用户信息接口返回的request_id为string类型
}

func NewAccountClient(accessToken string) *Account {
	return &Account{
		AccessToken: accessToken,
	}
}

// 获取网盘用户信息
func (a *Account) UserInfo() (UserInfoResponse, error) {
	ret := UserInfoResponse{}

	accessToken := a.AccessToken // string

	configuration := openapi.NewConfiguration()
	api_client := openapi.NewAPIClient(configuration)
	_, r, err := api_client.UserinfoApi.Xpannasuinfo(context.Background()).AccessToken(accessToken).Execute()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling `UserinfoApi.Xpannasuinfo``: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	}

	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(os.Stderr, "err: %v\n", r)
	}

	if err := json.Unmarshal(bodyBytes, &ret); err != nil {
		return ret, err
	}

	if ret.ErrorCode != 0 { //错误码不为0
		return ret, errors.New(fmt.Sprintf("error_code:%d, error_msg:%s", ret.ErrorCode, ret.ErrorMsg))
	}

	// 兼容用户信息接口返回的request_id为string类型的问题
	ret.RequestID, err = strconv.Atoi(ret.RequestIDStr)
	if err != nil {
		log.Println("strconv.Atoi failed, err:", err)
		return ret, err
	}

	return ret, nil
}
